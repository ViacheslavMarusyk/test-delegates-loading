import QtQuick 2.12
import QtQuick.Controls 2.5

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Scroll")

    ScrollView {
        anchors.fill: parent

        ListView {
            width: parent.width
            model: listModel

            delegate: Loader {
                id: _loader

                width: parent.width
                sourceComponent: component // Tab


                onLoaded: {
                    //print("loaded")
                    _loader.item.color = color;
                    _loader.item.text = color;
                }
            }
        }
    }
}
