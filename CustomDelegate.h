#ifndef CUSTOMDELEGATE_H
#define CUSTOMDELEGATE_H

#include <QObject>
#include <QQmlComponent>

class CustomDelegate : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QQmlComponent* component READ component CONSTANT)
    Q_PROPERTY(QString color READ color WRITE setColor NOTIFY colorChanged)
public:
    explicit CustomDelegate(QObject* parent = nullptr);
    QQmlComponent* component() const
    {
        return _component;
    }

    void setComponent(QQmlComponent* component)
    {
        _component = component;
    }

    QString color()
    {
        return _color;
    }

    void setColor(QString color)
    {
        if(_color != color)
        {
            _color = color;
            emit colorChanged();
        }
    }

signals:
    void colorChanged();
private:
    QQmlComponent* _component;
    QString _color = "white";
};

#endif // CUSTOMDELEGATE_H
