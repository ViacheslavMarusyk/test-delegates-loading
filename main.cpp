#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "CustomDelegate.h"
#include "dummymodel.h"
#include <chrono>
#include <QDebug>

using namespace std::chrono;

//#define DELEGATES_CREATED_ON_CPP

int main(int argc, char* argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    auto start = high_resolution_clock::now();

#ifdef DELEGATES_CREATED_ON_CPP

    DummyModel list(engine);
    engine.rootContext()->setContextProperty("listModel", QVariant::fromValue(&list));
    const QUrl url(QStringLiteral("qrc:/main.qml"));
#else
    engine.rootContext()->setContextProperty("listModel", DELEGATES_COUNT);
    const QUrl url(QStringLiteral("qrc:/main1.qml"));

#endif // DELEGATES_CREATED_ON_CPP

    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated, &app, [url](QObject * obj, const QUrl & objUrl)
    {
        if(!obj && url == objUrl)
        {
            QCoreApplication::exit(-1);
        }
    }, Qt::QueuedConnection);
    engine.load(url);

    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);

    qDebug() << "duration:" << duration.count();
    return app.exec();
}
