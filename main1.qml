import QtQuick 2.12
import QtQuick.Controls 2.5

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Scroll")

    ScrollView {
        anchors.fill: parent

        ListView {
            width: parent.width
            model: listModel

            delegate: Loader {
                id: _loader

                width: parent.width
                sourceComponent:  Tab {
                    color: index%2 == 0 ? "black" : "white"
                    text: color
                }
            }
        }
    }
}
