#ifndef DUMMYMODEL_H
#define DUMMYMODEL_H

#include <QAbstractListModel>
#include <QQmlApplicationEngine>

class QQmlComponent;

constexpr int DELEGATES_COUNT = 1000000;

class DummyModel : public QAbstractListModel
{
    Q_OBJECT
public:
    DummyModel(QObject *parent=nullptr);
    DummyModel(QQmlApplicationEngine &engine,
               QObject *parent=nullptr);

    // QAbstractItemModel interface
public:
//    virtual QModelIndex index(int row, int column, const QModelIndex &parent) const override;
    virtual QModelIndex parent(const QModelIndex &child) const override;
    virtual int rowCount(const QModelIndex &parent) const override;
    virtual int columnCount(const QModelIndex &parent) const override;
    virtual QVariant data(const QModelIndex &index, int role) const override;

    // QAbstractItemModel interface
public:
    virtual QHash<int, QByteArray> roleNames() const override;
private:
    QQmlComponent *_tabComponent;
};
Q_DECLARE_METATYPE(DummyModel*)
#endif // DUMMYMODEL_H
