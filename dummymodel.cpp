#include "dummymodel.h"
#include <QQmlComponent>

DummyModel::DummyModel(QObject *parent):
    QAbstractListModel(parent)
{

}

DummyModel::DummyModel(QQmlApplicationEngine &engine, QObject* parent):
    QAbstractListModel(parent)
{
    _tabComponent=new QQmlComponent(&engine,  "qrc:/Tab.qml");
}


//QModelIndex DummyModel::index(int row, int column, const QModelIndex &parent) const
//{
//    return QAbstractItemModel::index(row,column,parent);
//}

QModelIndex DummyModel::parent(const QModelIndex &child) const
{
    return QModelIndex();
}

int DummyModel::rowCount(const QModelIndex &parent) const
{
    return DELEGATES_COUNT;
}

int DummyModel::columnCount(const QModelIndex &parent) const
{
    return 1;
}

QVariant DummyModel::data(const QModelIndex &index, int role) const
{
    if(role==Qt::UserRole+1)
        return QVariant::fromValue(_tabComponent);
    else if(role==Qt::UserRole+2)
        return index.row() % 2 == 0 ? "black" : "white";
    else
        return QVariant();
}

QHash<int, QByteArray> DummyModel::roleNames() const
{
    QHash<int, QByteArray> names;
    names.insert(Qt::UserRole+1,"component");
    names.insert(Qt::UserRole+2,"color");

    return names;
}
